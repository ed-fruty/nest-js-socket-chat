import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import {ChatGateway} from './chat/chat.gateway';
import {ChatModule} from './chat/chat.module';
import {SimpleAuth} from './chat/services/auth/simple-auth';

@Module({
  imports: [ChatModule],
  controllers: [AppController],
  providers: [AppService, SimpleAuth, ChatGateway],
})
export class AppModule {}
