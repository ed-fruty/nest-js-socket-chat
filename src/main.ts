import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import {join} from 'path';
import {SocketIoAdapter} from './chat/socket/socket-io/socket-io-adapter';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.useStaticAssets(join(__dirname, '..', 'public'));
  app.setBaseViewsDir(join(__dirname, '..', 'views'));
  app.setViewEngine('hbs');

  app.useWebSocketAdapter(new SocketIoAdapter(app));

  await app.listen(3000);
}

bootstrap();
