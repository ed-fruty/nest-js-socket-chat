import { Injectable, MiddlewareFunction, NestMiddleware } from '@nestjs/common';

@Injectable()
export class AuthMiddlewareMiddleware implements NestMiddleware {

  resolve(...args: any[]) {
    return (socket, next) => {
      console.log('auth');

      return next();
    };
  }
}
