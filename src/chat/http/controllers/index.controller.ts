import {Controller, Get, Render} from '@nestjs/common';

@Controller()
export class IndexController {

    @Get('/welcome')
    @Render('welcome')
    welcome() {

    }

    @Get('/tool')
    @Render('tool')
    tool() {

    }
}