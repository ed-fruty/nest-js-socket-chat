export enum UserRole {
    OPERATOR = 'operator',
    CLIENT = 'client',
}