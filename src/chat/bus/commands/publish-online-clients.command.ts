import {SocketUser} from '../../models/socket-user';
import {ICommand} from '@nestjs/cqrs';

export class PublishOnlineClientsCommand implements ICommand {
    public constructor(public readonly operator: SocketUser) {}
}