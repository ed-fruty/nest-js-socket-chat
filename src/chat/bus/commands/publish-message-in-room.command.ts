import {ICommand} from '@nestjs/cqrs';
import {User} from '../../models/user';

export class PublishMessageInRoomCommand implements ICommand {
    public constructor(
        public readonly sender: User,
        public readonly message: string,
        public readonly room: string,
    ) {}
}