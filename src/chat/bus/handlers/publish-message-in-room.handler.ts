import {PublishMessageInRoomCommand} from '../commands/publish-message-in-room.command';
import {ICommandHandler, CommandHandler} from '@nestjs/cqrs';
import {UserDetails} from '../../models/user-details';
import {PublishMessageDto} from '../../dto/response/publish-message.dto';
import {ChatEvents} from '../../events/chat-events';

@CommandHandler(PublishMessageInRoomCommand)

export class PublishMessageInRoomHandler implements ICommandHandler<PublishMessageInRoomCommand> {

    /**
     * @param command
     * @param resolve
     */
    execute(command: PublishMessageInRoomCommand, resolve: (value?) => void): any {

        const publishMessage: PublishMessageDto = new PublishMessageDto(
            command.sender.details.get(UserDetails.USERNAME),
            command.sender.role,
            command.message,
        );

        command.sender.serverFacade.emitter.toRoom(
            command.room,
            ChatEvents.MESSAGE,
            publishMessage,
        );

        resolve();
    }
}