import {PublishMessageInRoomHandler} from './publish-message-in-room.handler';
import {PublishOnlineClientsHandler} from './publish-online-clients.handler';

export const CommandHandlers = [PublishMessageInRoomHandler, PublishOnlineClientsHandler];