import {PublishOnlineClientsCommand} from '../commands/publish-online-clients.command';
import {ICommandHandler, CommandHandler} from '@nestjs/cqrs';
import {ChatEvents} from '../../events/chat-events';
import {ForbiddenException} from '@nestjs/common';
import {OnlineUsersService} from '../../services/online-users.service';

@CommandHandler(PublishOnlineClientsCommand)
export class PublishOnlineClientsHandler implements ICommandHandler<PublishOnlineClientsCommand> {

    /**
     * @param onlineUsersService
     */
    public constructor(
        private readonly onlineUsersService: OnlineUsersService,
    ) {}

    /**
     * Send online clients list to the operator.
     *
     * @param command
     * @param resolve
     */
    public execute(command: PublishOnlineClientsCommand, resolve: (value?) => void) {

        // if (false === command.operator.isOperator) {
        //     throw new ForbiddenException();
        // }

        const socket = command.operator.socket;

        const onlineClients = this.onlineUsersService.getOnlineClients(socket.server);

        socket.emit(ChatEvents.ONLINE_CLIENTS, onlineClients);
        resolve();
    }
}