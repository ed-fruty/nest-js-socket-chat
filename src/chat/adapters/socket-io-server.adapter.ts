export class SocketIoServerAdapter {

    public constructor(public readonly server: any) {}

    public connections(room: string | null = null, namespace: string = '/') {
        const sockets = this.server.of(namespace).clients(room).sockets;

        return Object.keys(sockets).map(key => sockets[key]);
    }

    public connectionsBy(filter: (connection) => true, room: string | null = null, namespace: string = '/') {
        const connections = [];
        const sockets = this.server.of(namespace).clients(room).sockets;

        for (const index of sockets) {
            if (filter(sockets[index])) {
                connections.push(sockets[index]);
            }
        }

        return connections;
    }

    public operatorsConnections(room: string | null = null, namespace: string = '/') {
        return this.connectionsBy(connection => connection.user.isOperator, room , namespace);
    }

    public clientsConnections(room: string | null = null, namespace: string = '/') {
        return this.connectionsBy(connection => connection.user.isClient, room, namespace);
    }

    public users(room: string | null = null, namespace: string = '/') {
        return _.unique(this.connections(room, namespace), SocketIoServerAdapter.isEquals);
    }

    public usersBy(filter: (user) => true, room: string | null = null, namespace: string = '/') {
        const users = [];

        const sockets = this.server.of(namespace).clients(room).sockets;

        for (const index of sockets) {
            const user = sockets[index].user;

            if (filter(user)) {
                users.push(user);
            }
        }

        return _.unique(users, SocketIoServerAdapter.isEquals);
    }

    public operators(room: string | null = null, namespace: string = '/') {
        return this.usersBy(user => user.isOperator, room, namespace);
    }

    public clients(room: string | null = null, namespace: string = '/') {
        return this.usersBy(user => user.isClient, room, namespace);
    }

    toOperatorsInRoom(event: string, payload: any, room: string, namespace: string = '/') {

        const connections = this.operatorsConnections(room, namespace)
            .map(connection => connection.id);

        return this.server
            .to(connections)
            .emit(event, payload);
    }

    broadcastToOperatorsInRoom(user: User, event: string, payload: any, room: string, namespace: string = '/') {
        const connections = this.operatorsConnections(room, namespace)
            .map(connection => connection.id);

        return user.socket.server.broadcast
            .to(connections)
            .emit(event, payload);
    }

    private static isEquals(a, b): boolean {
        return (a.isClient && b.isClient) && (a.username === b.username);
    }
}