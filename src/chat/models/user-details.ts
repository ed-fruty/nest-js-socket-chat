export class UserDetails {

    static readonly USERNAME: string = 'username';
    static readonly EMAIL: string = 'email';
    static readonly ID: string = 'id';

    /**
     * @param data
     */
    public constructor(
        private readonly data: object,
    ) {}

    /**
     * Get user attribute from details.
     *
     * @param key
     * @param defaultValue
     */
    public get(key: string, defaultValue: any = ''): string {
        if (typeof this.data[key] === 'undefined') {
            return String(defaultValue);
        }
        return String(this.data[key]);
    }

    /**
     * Create details from socket handshake.
     *
     * @param socket
     */
    public static createFromSocket(socket: any) {
        return new UserDetails(socket.handshake.query);
    }
}