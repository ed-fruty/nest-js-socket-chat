import {UserDetails} from './user-details';
import {SocketServerFacadeInterface} from '../socket/contracts/socket-server-facade.interface';
import {UserRole} from '../enums/user-roles.enum';

export class User {

    public readonly details: UserDetails;
    public readonly socket: any;
    public readonly activeClientRoom: string | null;
    public readonly serverFacade: SocketServerFacadeInterface;
    public readonly isClient: boolean;
    public readonly isOperator: boolean;
    public readonly role: UserRole;

    public isWaitingForAnswer = false;

    /**
     *
     * @param socket
     * @param isClient
     * @param activeClientRoom
     */
    public constructor(
        socket: any,
        isClient: boolean = true,
        activeClientRoom: string | null = null,
        ) {
        this.details = UserDetails.createFromSocket(socket);
        this.socket = socket;
        this.serverFacade = socket.server.facade;
        this.activeClientRoom = activeClientRoom;

        this.isClient = isClient;
        this.isOperator = !isClient;
        this.role = isClient ? UserRole.CLIENT : UserRole.OPERATOR;
    }

    /**
     * Get list of user connections.
     */
    public connections(): Array<any> {
        return this.serverFacade.connections.forUser(this);
    }

    /**
     * Get username.
     */
    public getUsername(): string {
        return String(this.details.get(UserDetails.USERNAME));
    }

    /**
     * Get primary key
     */
    public getKey() {
        return this.details.get(UserDetails.ID);
    }

    /**
     * Detect room by user type and given room.
     *
     * @param room
     */
    public detectRoom(room?: string) {
        if (this.isClient) {
            return this.activeClientRoom;
        }

        return room;
    }
}