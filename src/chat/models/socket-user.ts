import {UserDetails} from './user-details';
import {UserRole} from '../enums/user-roles.enum';

export class SocketUser {

    public readonly details: UserDetails;
    public readonly socket;
    public readonly isOperator: boolean;
    public readonly isClient: boolean;
    public readonly role: UserRole;

    /**
     * @param socket
     */
    public constructor(socket) {
        this.details = new UserDetails(socket.handshake.query || {});

        this.socket = socket;

        if (typeof socket.isOperator !== 'undefined' && true === socket.isOperator)    {
            this.isOperator = true;
            this.isClient = false;
            this.role = UserRole.OPERATOR;
        } else {
            this.isClient = true;
            this.isOperator = false;
            this.role = UserRole.CLIENT;
        }
    }

    /**
     * Get client active room
     */
    public getClientRoom(): string {
        return Object.keys(this.socket.rooms)[0];
    }
}