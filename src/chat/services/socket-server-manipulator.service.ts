import {SocketUser} from '../models/socket-user';
import {Injectable} from '@nestjs/common';

@Injectable()
export class SocketServerManipulatorService {

    private server;

    /**
     * Set socket server instance.
     *
     * @param server
     */
    public setServer(server) {
        this.server = server;

        return this;
    }

    /**
     * Get clients list
     */
    public clients(): Array<SocketUser> {
        const clients = this.server.clients();

        return Object.keys(clients.sockets)
            .map(key => new SocketUser(clients.sockets[key]));
    }
}