export interface AuthServiceInterface {
    /**
     *
     * @param signature
     * @param username
     * @param query
     * @param token
     * @param room
     */
    auth(signature: string, username: string, query: object, token?: string, room?: string);
}