import {AuthServiceInterface} from './auth-service.interface';
import {User} from '../../models/user';
import * as uuid from 'uuid';
import {Injectable} from '@nestjs/common';

@Injectable()
export class SimpleAuth {

    integrityCheck(signature: string, params: object) {
        return true;
    }

    auth(socket): User {
        const token = socket.handshake.query.token;
        const isOperator = token === '1111';
        const activeRoom = isOperator ? null : uuid.v4();

        if (activeRoom) {
            socket.join(activeRoom);
        }

        socket.user = new User(socket, !isOperator, activeRoom);

        return socket.user;
    }
}