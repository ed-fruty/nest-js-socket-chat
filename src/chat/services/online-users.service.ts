import {SocketServerManipulatorService} from './socket-server-manipulator.service';
import {OnlineClientDto} from '../dto/response/online-client.dto';
import {UserDetails} from '../models/user-details';
import {Injectable} from '@nestjs/common';

@Injectable()
export class OnlineUsersService {

    /**
     * @param manipulator
     */
    public constructor(
        private readonly manipulator: SocketServerManipulatorService,
    ) {}

    /**
     * Get list of online clients mapped to OnlineClientDto
     */
    public getOnlineClients(server): Array<OnlineClientDto> {
        return this.manipulator.setServer(server)
            .clients()
            .filter(client => client.isClient)
            .map(client => {
                return new OnlineClientDto(
                    client.details.get(UserDetails.USERNAME),
                    client.getClientRoom(),
                );
            });
    }
}