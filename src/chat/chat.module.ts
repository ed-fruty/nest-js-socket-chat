import {Module, OnModuleInit} from '@nestjs/common';
import {ModuleRef} from '@nestjs/core';
import {CommandBus, EventBus, CQRSModule} from '@nestjs/cqrs';
import {CommandHandlers} from './bus/handlers';
import {IndexController} from './http/controllers/index.controller';
import {OnlineUsersService} from './services/online-users.service';
import {SocketServerManipulatorService} from './services/socket-server-manipulator.service';

@Module({
    imports: [CQRSModule],
    controllers: [IndexController],
    providers: [
        ...CommandHandlers,
        SocketServerManipulatorService,
        OnlineUsersService,
    ],
})
export class ChatModule implements OnModuleInit{
    public constructor(
        private readonly moduleRef: ModuleRef,
        private readonly command$: CommandBus,
        private readonly event$: EventBus,
    ) {}

    onModuleInit() {
        this.command$.setModuleRef(this.moduleRef);
        this.event$.setModuleRef(this.moduleRef);

        this.command$.register(CommandHandlers);
    }
}