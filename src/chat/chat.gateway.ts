import {OnGatewayConnection, SubscribeMessage, WebSocketGateway, WebSocketServer} from '@nestjs/websockets';
import {CommandBus} from '@nestjs/cqrs';
import {PublishMessageInRoomCommand} from './bus/commands/publish-message-in-room.command';
import {Body, Inject, UsePipes, ValidationPipe} from '@nestjs/common';
import {MessageDto} from './dto/request/message.dto';
import {SocketUser} from './models/socket-user';
import {ChatEvents} from './events/chat-events';
import {PublishOnlineClientsCommand} from './bus/commands/publish-online-clients.command';
import {SimpleAuth} from './services/auth/simple-auth';

@UsePipes(new ValidationPipe({
    // whitelist: true,
    transform: true,
}))
@WebSocketGateway(Number(process.env.PORT) || null, {upgrade: true})
export class ChatGateway implements OnGatewayConnection {

    @WebSocketServer()
    private readonly server;

    /**
     * @param commandBus
     * @param authService
     */
    public constructor(
        private readonly commandBus: CommandBus,
        private readonly authService: SimpleAuth,
    ) {}

    /**
     * Publish message for all room connections.
     *
     * @param socket
     * @param message
     */
    @SubscribeMessage(ChatEvents.MESSAGE)
    public async message(socket, @Body() message: MessageDto) {

        const command: PublishMessageInRoomCommand = new PublishMessageInRoomCommand(
            socket.user,
            message.content,
            socket.user.detectRoom(message.room),
        );

        await this.commandBus.execute(command);
    }

    /**
     * Publish all online clients for given connection.
     *
     * @param socket
     */
    @SubscribeMessage(ChatEvents.ONLINE_CLIENTS)
    public async onlineClients(socket) {

        const operator = new SocketUser(socket);
        const command: PublishOnlineClientsCommand = new PublishOnlineClientsCommand(operator);

        /** @see PublishOnlineClientsHandler.execute */
        await this.commandBus.execute(command);
    }

    /**
     * Handle and authorize connection.
     *
     * @param client
     * @param args
     */
    handleConnection(client: any, ...args: any[]): any {

        this.authService.auth(client);
    }
}
