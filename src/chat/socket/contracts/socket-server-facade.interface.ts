import {ConnectionManagerInterface} from './connection-manager.interface';
import {UserManagerInterface} from './user-manager.interface';
import {EmitterInterface} from './emitter.interface';
import {BroadcasterInterface} from './broadcaster.interface';

export interface SocketServerFacadeInterface {

    readonly server: any;

    readonly connections: ConnectionManagerInterface;

    readonly users: UserManagerInterface;

    readonly emitter: EmitterInterface;

    readonly broadcaster: BroadcasterInterface;
}