import {SocketServerFacadeInterface} from './socket-server-facade.interface';
import {User} from '../../models/user';

export interface ConnectionManagerInterface {

    /**
     * Get all connections list.
     *
     * @param room
     * @param namespace
     */
    all(room?: string | null, namespace: string = '/'): Array<any>;

    /**
     *  Get connections filtered by a given condition.
     *
     * @param filter
     * @param room
     * @param namespace
     */
    filter(filter: (connection) => any, room: string | null = null, namespace: string = '/'): Array<any>;

    /**
     * Filter and map connections in one iteration.
     *
     * @param filter
     * @param map
     * @param room
     * @param namespace
     */
    filterAndMap(filter: (connection) => any, map: (connection) => any, room?: string | null, namespace: string = '/'): Array<any>;

    /**
     * Get only operators connections.
     *
     * @param room
     * @param namespace
     */
    operators(room: string | null = null, namespace: string = '/'): Array<any>;

    /**
     *
     * @param filter
     * @param room
     * @param namespace
     */
    operatorsBy(filter: (connection) => any, room: string | null = null, namespace: string = '/'): Array<any>;

    /**
     * Get filtered operators connections and map them with given map callback.
     *
     * @param filter
     * @param map
     * @param room
     * @param namespace
     */
    operatorsByFilterAndMap(filter: (connection) => any, map: (connection) => any, room?: string | null, namespace: string = '/'): Array<any>;

    /**
     * Get connections list for given user.
     *
     * @param user
     */
    forUser(user: User): Array<any>;

    /**
     * Get only clients connections.
     *
     * @param room
     * @param namespace
     */
    clients(room: string | null = null, namespace: string = '/'): Array<any>;

    /**
     * Get filtered clients connections.
     *
     * @param filter
     * @param room
     * @param namespace
     */
    clientsBy(filter: (connection) => any, room: string | null = null, namespace: string = '/'): Array<any>;

    /**
     *
     * @param filter
     * @param map
     * @param room
     * @param namespace
     */
    clientsByFilterAndMap(filter: (connection) => any, map: (connection) => any, room?: string | null, namespace: string = '/'): Array<any>;

    /**
     * Bootstrap connection manager
     *
     * @param facade
     */
    bootstrap(facade: SocketServerFacadeInterface);
}