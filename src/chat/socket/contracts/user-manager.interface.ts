import {User} from '../../models/user';
import {SocketServerFacadeInterface} from './socket-server-facade.interface';

export interface UserManagerInterface {

    /**
     * Get all users.
     *
     * @param room
     * @param namespace
     */
    all(room: string | null = null, namespace: string = '/'): Array<User>;

    /**
     * Get only operators users.
     *
     * @param room
     * @param namespace
     */
    operators(room: string | null = null, namespace: string = '/'): Array<User>;

    /**
     * Get only clients users.
     *
     * @param room
     * @param namespace
     */
    clients(room: string | null = null, namespace: string = '/'): Array<User>;

    /**
     * Bootstrap user manager instance.
     *
     * @param facade
     */
    bootstrap(facade: SocketServerFacadeInterface);
}