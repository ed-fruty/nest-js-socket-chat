import {User} from '../../models/user';
import {SocketServerFacadeInterface} from './socket-server-facade.interface';

export interface EmitterInterface {

    /**
     * Emit event to all connections in the room.
     *
     * @param room
     * @param event
     * @param payload
     * @param namespace
     */
    toRoom(room: string, event: string, payload: any, namespace?: string);

    /**
     * Emit event only for operator connections in the room.
     *
     * @param room
     * @param event
     * @param payload
     * @param namespace
     */
    toOperatorsInRoom(room: string, event: string, payload: any, namespace?: string);

    /**
     * Emit event only for clients connections in the room.
     *
     * @param room
     * @param event
     * @param payload
     * @param namespace
     */
    toClientsInRoom(room: string, event: string, payload: any, namespace?: string);

    /**
     * Emit event to all operator connections by given operator instance.
     *
     * @param operator
     * @param event
     * @param payload
     * @param namespace
     */
    toOperator(operator: User, event: string, payload: any, namespace?: string);

    /**
     * Emit event to all client connections by given client instance.
     *
     * @param client
     * @param event
     * @param payload
     * @param namespace
     */
    toClient(client: User, event: string, payload: any, namespace?: string);

    /**
     * Emit event for client connections if them in the given room.
     *
     * @param client
     * @param room
     * @param event
     * @param payload
     * @param namespace
     */
    toClientInRoom(client: User, room: string, event: string, payload: any, namespace?: string);

    /**
     * Emit event to all operators in any rooms.
     *
     * @param event
     * @param payload
     * @param namespace
     */
    toAllOperators(event: string, payload: any, namespace?: string);

    /**
     * Emit event to specific operator connections if them in the given room.
     *
     * @param operator
     * @param room
     * @param event
     * @param payload
     * @param namespace
     */
    toOperatorInRoom(operator: User, room: string, event: string, payload: any, namespace?: string);

    /**
     * Emit event to all clients in any rooms.
     *
     * @param event
     * @param payload
     * @param namespace
     */
    toAllClients(event: string, payload: any, namespace?: string);

    /**
     * Emit event to concrete user.
     *
     * @param user
     * @param event
     * @param payload
     * @param namespace
     */
    toUser(user: User, event: string, payload: any, namespace?: string);

    /**
     * Emit event to concrete user connections in the given room.
     *
     * @param user
     * @param room
     * @param event
     * @param payload
     * @param namespace
     */
    toUserInRoom(user: User, room: string, event: string, payload: any, namespace?: string);

    /**
     * Bootstrap emitter instance.
     *
     * @param facade
     */
    bootstrap(facade: SocketServerFacadeInterface);
}