
import {SocketServerFacadeInterface} from './socket-server-facade.interface';
import {User} from '../../models/user';

export interface BroadcasterInterface {

    /**
     * Broadcast event to the room.
     *
     * @param broadcaster
     * @param room
     * @param event
     * @param payload
     * @param namespace
     */
    toRoom(broadcaster: User, room: string, event: string, payload: any, namespace?: string);

    /**
     * Broadcast event only to operators connections in the room.
     *
     * @param broadcaster
     * @param room
     * @param event
     * @param payload
     * @param namespace
     */
    toOperatorsInRoom(broadcaster: User, room: string, event: string, payload: any, namespace?: string);

    /**
     * Broadcast event only to clients connections in the room.
     *
     * @param broadcaster
     * @param room
     * @param event
     * @param payload
     * @param namespace
     */
    toClientsInRoom(broadcaster: User, room: string, event: string, payload: any, namespace?: string);

    /**
     * Broadcast event to all operators connections in any room.
     *
     * @param broadcaster
     * @param event
     * @param payload
     * @param namespace
     */
    toAllOperators(broadcaster: User, event: string, payload: any, namespace?: string);

    /**
     * Broadcast event to all clients connections in any room.
     *
     * @param broadcaster
     * @param event
     * @param payload
     * @param namespace
     */
    toAllClients(broadcaster: User, event: string, payload: any, namespace?: string);

    /**
     * Broadcast event to all connections in all rooms.
     *
     * @param broadcaster
     * @param event
     * @param payload
     * @param namespace
     */
    toAll(broadcaster: User, event: string, payload: any, namespace?: string);

    /**
     * Bootstrap broadcaster.
     *
     * @param facade
     */
    bootstrap(facade: SocketServerFacadeInterface);
}