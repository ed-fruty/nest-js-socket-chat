import {UserManagerInterface} from '../contracts/user-manager.interface';
import {User} from '../../models/user';
import {SocketServerFacadeInterface} from '../contracts/socket-server-facade.interface';
import _ from 'lodash';

export class UserManager implements UserManagerInterface {

    private serverFacade: SocketServerFacadeInterface;

    /**
     * Get all users.
     *
     * @param room
     * @param namespace
     */
    all(room?: string | null, namespace: string = '/'): Array<User> {
        return _.uniqWith(
            this.serverFacade.connections.all(room, namespace),
            UserManager.isEquals,
        );
    }

    /**
     * Get only clients users.
     *
     * @param room
     * @param namespace
     */
    clients(room?: string | null, namespace: string = '/'): Array<User> {
        return _.uniqWith(
            this.serverFacade.connections.clients(room, namespace),
            UserManager.isEquals,
        );
    }

    /**
     * Get only operators users.
     *
     * @param room
     * @param namespace
     */
    operators(room?: string | null, namespace: string = '/'): Array<User> {
        return _.uniqWith(
            this.serverFacade.connections.operators(room, namespace),
            UserManager.isEquals,
        );
    }

    /**
     * Set socket server facade instance.
     *
     * @param facade
     */
    bootstrap(facade: SocketServerFacadeInterface) {
        this.serverFacade = facade;
    }

    /**
     * Check is two connections are equals.
     *
     * @param a
     * @param b
     * @return boolean
     */
    private static isEquals(a, b): boolean {
        return a.user.isClient === b.user.isClient && a.user.getUsername() === b.user.getUsername();
    }

}