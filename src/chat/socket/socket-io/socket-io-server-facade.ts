import {ConnectionManagerInterface} from '../contracts/connection-manager.interface';
import {UserManagerInterface} from '../contracts/user-manager.interface';
import {EmitterInterface} from '../contracts/emitter.interface';
import {BroadcasterInterface} from '../contracts/broadcaster.interface';
import {SocketServerFacadeInterface} from '../contracts/socket-server-facade.interface';

export class SocketIoServerFacade implements SocketServerFacadeInterface {

    readonly server: any;
    readonly connections: ConnectionManagerInterface;
    readonly users: UserManagerInterface;
    readonly emitter: EmitterInterface;
    readonly broadcaster: BroadcasterInterface;

    public static instance: SocketIoServerFacade;

    /**
     * @param server
     * @param connectionManager
     * @param userManager
     * @param emitter
     * @param broadcaster
     */
    public constructor(
        server: any,
        connectionManager: ConnectionManagerInterface,
        userManager: UserManagerInterface,
        emitter: EmitterInterface,
        broadcaster: BroadcasterInterface,
    ) {
        connectionManager.bootstrap(this);
        userManager.bootstrap(this);
        emitter.bootstrap(this);
        broadcaster.bootstrap(this);

        this.connections = connectionManager;
        this.users = userManager;
        this.emitter = emitter;
        this.broadcaster = broadcaster;
        this.server = server;

        SocketIoServerFacade.instance = this;
    }
}