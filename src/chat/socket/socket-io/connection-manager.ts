import {ConnectionManagerInterface} from '../contracts/connection-manager.interface';
import {User} from '../../models/user';
import {SocketServerFacadeInterface} from '../contracts/socket-server-facade.interface';

export class ConnectionManager implements ConnectionManagerInterface {

    private server: any;

    /**
     * Get all connections list.
     *
     * @param room
     * @param namespace
     */
    all(room?: string | null, namespace: string = '/'): Array<any> {
        const sockets = this.server.of(namespace).clients(room).sockets;

        return Object.keys(sockets).map(key => sockets[key]);
    }

    /**
     * Get only clients connections.
     *
     * @param room
     * @param namespace
     */
    clients(room?: string | null, namespace: string = '/'): Array<any> {
        return this.filter(connection => connection.user.isClient, room, namespace);
    }

    /**
     * Get filtered clients connections.
     *
     * @param filter
     * @param room
     * @param namespace
     */
    clientsBy(filter: (connection) => any, room?: string | null, namespace?: string): Array<any> {
        return this.filter((connection) => {
            return connection.user.isClient && filter(connection);
        }, room, namespace);
    }

    /**
     *  Get connections filtered by a given condition.
     *
     * @param filter
     * @param room
     * @param namespace
     */
    filter(filter: (connection) => any, room?: string | null, namespace: string = '/'): Array<any> {
        return this.filterAndMap(filter, (connection) => connection, room, namespace);
    }

    /**
     * Get connections list for given user.
     *
     * @param user
     */
    forUser(user: User): Array<any> {
        return this.filter((connection) => {
            return connection.user.isClient === user.isClient && connection.user.getUsername() === user.getUsername();
        });
    }

    /**
     * Get only operators connections.
     *
     * @param room
     * @param namespace
     */
    operators(room?: string | null, namespace: string = '/'): Array<any> {
        return this.filter(connection => connection.user.isOperator, room, namespace);
    }

    /**
     * Get filtered operators connections.
     *
     * @param filter
     * @param room
     * @param namespace
     */
    operatorsBy(filter: (connection) => any, room?: string, namespace: string = '/') {
        return this.filter((connection) => {
            return connection.user.isOperator && filter(connection);
        }, room, namespace);
    }

    /**
     * Bootstrap connection manager
     *
     * @param facade
     */
    bootstrap(facade: SocketServerFacadeInterface) {
        this.server = facade.server;
    }

    /**
     * Get clients connections filtered and map in one iteration for connection.
     *
     * @param filter
     * @param map
     * @param room
     * @param namespace
     */
    clientsByFilterAndMap(filter: (connection) => any, map: (connection) => any, room?: string | null, namespace?: string): Array<any> {
        return this.filterAndMap((connection) => {
            return connection.user.isClient && Boolean(filter(connection));
        }, map, room, namespace);
    }

    /**
     * Get connections filtered and map in one iteration for connection.
     *
     * @param filter
     * @param map
     * @param room
     * @param namespace
     */
    filterAndMap(filter: (connection) => any, map: (connection) => any, room?: string | null, namespace: string = '/'): Array<any> {

        const connections = [];
        // const sockets = this.server.of(namespace).clients(room).sockets;

        if (! room) {
            const sockets = this.server.of(namespace).adapter.nsp.connected;

            for (const index of sockets) {
                if (filter(sockets[index])) {
                    connections.push(map(sockets[index]));
                }
            }

        } else {
            const sockets = this.server.of(namespace).rooms[room].sockets;

            for (const index of sockets) {
                const socket = this.server.of(namespace).connected[index];

                if (socket && filter(socket)) {
                    connections.push(map(socket));
                }
            }
        }

        return connections;
    }

    /**
     * Get operators connections filtered and map in one iteration for connection.
     *
     * @param filter
     * @param map
     * @param room
     * @param namespace
     */
    operatorsByFilterAndMap(filter: (connection) => any, map: (connection) => any, room?: string | null, namespace: string = '/'): Array<any> {
        return this.filterAndMap(
            connection => connection.user.isOperator && filter(connection),
            map,
            room,
            namespace,
        );
    }
}