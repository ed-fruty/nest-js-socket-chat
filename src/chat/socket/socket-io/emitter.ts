import {EmitterInterface} from '../contracts/emitter.interface';
import {SocketServerFacadeInterface} from '../contracts/socket-server-facade.interface';
import {User} from '../../models/user';
import {ForbiddenException} from '@nestjs/common';
import {InvalidUserTypeExceptionException} from '../../exceptions/invalid-user-type-exception.exception';

export class Emitter implements EmitterInterface {

    private serverFacade: SocketServerFacadeInterface;

    /**
     * Bootstrap emitter instance.
     *
     * @param facade
     */
    bootstrap(facade: SocketServerFacadeInterface) {
        this.serverFacade = facade;
    }

    /**
     * Emit event to all clients in any rooms.
     *
     * @param event
     * @param payload
     * @param namespace
     */
    toAllClients(event: string, payload: any, namespace?: string) {
        const connections = this.serverFacade.connections.clients(null, namespace)
            .map(connection => connection.id);

        this.serverFacade.server
            .to(connections)
            .emit(event, payload);
    }

    /**
     * Emit event to all operators in any rooms.
     *
     * @param event
     * @param payload
     * @param namespace
     */
    toAllOperators(event: string, payload: any, namespace?: string) {
        const connections = this.serverFacade.connections.operators(null, namespace)
            .map(connection => connection.id);

        this.serverFacade.server
            .to(connections)
            .emit(event, payload);
    }

    /**
     * Emit event to all client connections by given client instance.
     *
     * @param client
     * @param event
     * @param payload
     * @param namespace
     */
    toClient(client: User, event: string, payload: any, namespace?: string) {
        if (false === client.isClient) {
            throw new ForbiddenException('Can\'t emit to client. Given user is not client');
        }

        return this.toUser(client, event, payload, namespace);
    }

    /**
     * Emit event only for clients connections in the room.
     *
     * @param room
     * @param event
     * @param payload
     * @param namespace
     */
    toClientsInRoom(room: string, event: string, payload: any, namespace?: string) {
    }

    /**
     * Emit event to all operator connections by given operator instance.
     *
     * @param operator
     * @param event
     * @param payload
     * @param namespace
     */
    toOperator(operator: User, event: string, payload: any, namespace?: string) {
        if (false === operator.isOperator) {
            throw new InvalidUserTypeExceptionException('Can\'t emit to operator. Given user is not operator');
        }

        return this.toUser(operator, event, payload, namespace);
    }

    /**
     * Emit event only for operator connections in the room.
     *
     * @param room
     * @param event
     * @param payload
     * @param namespace
     */
    toOperatorsInRoom(room: string, event: string, payload: any, namespace?: string) {
        const connections = this.serverFacade.connections.operators(room, namespace)
            .map(connection => connection.id);

        this.serverFacade.server
            .in(connections)
            .emit(event, payload);
    }

    /**
     * Emit event to all connections in the room.
     *
     * @param room
     * @param event
     * @param payload
     * @param namespace
     */
    toRoom(room: string, event: string, payload: any, namespace: string = '/') {

        this.serverFacade.server
            .of(namespace)
            .in(room)
            .emit(event, payload);
    }

    /**
     * Emit event to specific user connections.
     *
     * @param user
     * @param event
     * @param payload
     * @param namespace
     */
    toUser(user: User, event: string, payload: any, namespace?: string) {
        const connections = user.connections()
            .map(connection => connection.id);

        this.serverFacade.server
            .of(namespace)
            .to(connections)
            .emit(event, payload);
    }

    /**
     * Emit event for specific user connections in the given room.
     *
     * @param user
     * @param room
     * @param event
     * @param payload
     * @param namespace
     */
    toUserInRoom(user: User, room: string, event: string, payload: any, namespace?: string) {

        const connections = this.serverFacade.connections.filterAndMap(
            connection => connection.user.getUsername() === user.getUsername(),
            connection => connection.id,
            room,
            namespace,
        );

        this.serverFacade.server
            .to(connections)
            .emit(event, payload);
    }

    /**
     * Emit event to specific client connections if them in the given room.
     *
     * @param client
     * @param room
     * @param event
     * @param payload
     * @param namespace
     */
    toClientInRoom(client: User, room: string, event: string, payload: any, namespace?: string) {
        if (false === client.isClient) {
            throw new InvalidUserTypeExceptionException('Can\'t emit to client. Given user is not client');
        }

        const connections = this.serverFacade.connections.clientsByFilterAndMap(
            connection => connection.user.getUsername() === client.getUsername(),
            connection => connection.id,
            room,
            namespace,
        );

        this.serverFacade.server
            .to(connections)
            .emit(event, payload);
    }

    /**
     * Emit event to specific operator connections if them in the given room.
     *
     * @param operator
     * @param room
     * @param event
     * @param payload
     * @param namespace
     */
    toOperatorInRoom(operator: User, room: string, event: string, payload: any, namespace?: string) {
        if (false === operator.isOperator) {
            throw new InvalidUserTypeExceptionException('Can\'t emit to operator. Given user is not operator');
        }

        const connections = this.serverFacade.connections.operatorsByFilterAndMap(
            connection => connection.user.getUsername() === operator.getUsername(),
            connection => connection.id,
            room,
            namespace,
        );

        this.serverFacade.server
            .to(connections)
            .emit(event, payload);
    }

}