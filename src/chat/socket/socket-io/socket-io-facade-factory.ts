import {SocketIoServerFacade} from './socket-io-server-facade';
import {ConnectionManager} from './connection-manager';
import {UserManager} from './user-manager';
import {Emitter} from './emitter';
import {Broadcaster} from './broadcaster';

export class SocketIoFacadeFactory {

    /**
     * Create socket server facade.
     *
     * @param server
     */
    public static create(server: any) {
        return new SocketIoServerFacade(
            server,
            new ConnectionManager(),
            new UserManager(),
            new Emitter(),
            new Broadcaster(),
        );
    }
}