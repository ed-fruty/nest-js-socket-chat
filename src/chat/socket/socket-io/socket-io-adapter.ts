import { IoAdapter } from '@nestjs/websockets';
import * as redisIoAdapter from 'socket.io-redis';
import {SocketIoFacadeFactory} from './socket-io-facade-factory';

const redisAdapter = redisIoAdapter({ host: 'localhost', port: 6379 });

export class SocketIoAdapter extends IoAdapter {

    /**
     * Create IO server.
     *
     * @param port
     * @param options
     */
    createIOServer(port: number, options?: any): any {
        const server = super.createIOServer(port, options);

        server.adapter(redisAdapter);
        server.facade = SocketIoFacadeFactory.create(server);

        return server;
    }
}