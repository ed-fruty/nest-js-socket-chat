import {BroadcasterInterface} from '../contracts/broadcaster.interface';
import {SocketServerFacadeInterface} from '../contracts/socket-server-facade.interface';
import {User} from '../../models/user';

export class Broadcaster implements BroadcasterInterface {

    private serverFacade: SocketServerFacadeInterface;

    /**
     * Bootstrap broadcaster.
     *
     * @param facade
     */
    bootstrap(facade: SocketServerFacadeInterface) {
        this.serverFacade = facade;
    }

    /**
     * Broadcast event to all clients connections in any room.
     *
     * @param broadcaster
     * @param event
     * @param payload
     * @param namespace
     */
    toAllClients(broadcaster: User, event: string, payload: any, namespace: string = '/') {
        // const connections = this.serverFacade.connections.clients(null, namespace)
        //     .filter((connection) => connection.user.getUsername() !== broadcaster.getUsername(),)
        //     .map(connection => connection.id);

        const connections = this.serverFacade.connections.clientsByFilterAndMap(
            connection => connection.user.getUsername() !== broadcaster.getUsername(),
            connection => connection.id,
            null,
            namespace,
        );

        this.serverFacade.server
            .to(connections)
            .emit(event, payload);
    }

    /**
     * Broadcast event to all operators connections in any room.
     *
     * @param broadcaster
     * @param event
     * @param payload
     * @param namespace
     */
    toAllOperators(broadcaster: User, event: string, payload: any, namespace: string = '/') {

        const connections = this.serverFacade.connections.operatorsByFilterAndMap(
            connection => connection.user.getUsername() !== broadcaster.getUsername(),
            connection => connection.id,
            null,
            namespace,
        );

        this.serverFacade.server
            .to(connections)
            .emit(event, payload);
    }

    /**
     * Broadcast event only to clients connections in the room.
     *
     * @param broadcaster
     * @param room
     * @param event
     * @param payload
     * @param namespace
     */
    toClientsInRoom(broadcaster: User, room: string, event: string, payload: any, namespace: string = '/') {

        const connections = this.serverFacade.connections.clientsByFilterAndMap(
            connection => connection.user.getUsername() !== broadcaster.getUsername(),
            connection => connection.id,
            room,
            namespace,
        );

        this.serverFacade.server
            .to(connections)
            .emit(event, payload);
    }

    /**
     * Broadcast event only to operators connections in the room.
     *
     * @param broadcaster
     * @param room
     * @param event
     * @param payload
     * @param namespace
     */
    toOperatorsInRoom(broadcaster: User, room: string, event: string, payload: any, namespace: string = '/') {

        const connections = this.serverFacade.connections.operatorsByFilterAndMap(
            connection => connection.user.getUsername() !== broadcaster.getUsername(),
            connection => connection.id,
            room,
            namespace,
        );

        this.serverFacade.server
            .to(connections)
            .emit(event, payload);
    }

    /**
     * Broadcast event to the room.
     *
     * @param broadcaster
     * @param room
     * @param event
     * @param payload
     * @param namespace
     */
    toRoom(broadcaster: User, room: string, event: string, payload: any, namespace: string = '/') {

        // const connections = this.serverFacade.connections.filterAndMap(
        //     connection => connection.user.getUsername() !== broadcaster.getUsername(),
        //     connection => connection.id,
        //     room,
        //     namespace,
        // );
        //
        // this.serverFacade.server
        //     .to(connections)
        //     .emit(event, payload);

        broadcaster.socket.broadcast
            .to(room)
            .emit(event, payload);
    }

    /**
     * Broadcast message to all connections. Don't use it without really needs.
     *
     * @param broadcaster
     * @param event
     * @param payload
     * @param namespace
     */
    toAll(broadcaster: User, event: string, payload: any, namespace: string = '/') {
        broadcaster.socket.broadcast.emit(event, payload);
    }
}