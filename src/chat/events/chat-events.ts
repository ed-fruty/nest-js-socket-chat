export enum ChatEvents {
    MESSAGE = 'message',
    ONLINE_CLIENTS = 'onlineClients',
}