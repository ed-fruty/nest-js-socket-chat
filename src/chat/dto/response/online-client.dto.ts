export class OnlineClientDto {
    public constructor(
        public readonly username: string,
        public readonly room: string,
    ) {}
}