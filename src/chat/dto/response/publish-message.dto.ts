export class PublishMessageDto {
    public constructor(
        public readonly author: string,
        public readonly role: string,
        public readonly content: string,
    ) {}
}