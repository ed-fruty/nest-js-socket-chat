import {IsNotEmpty, IsString} from 'class-validator';

export class MessageDto {

    @IsNotEmpty()
    @IsString()
    readonly content: string;

    readonly room: string;
}